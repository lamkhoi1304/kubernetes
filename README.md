# KUBERNETES PROJECT
Setup infrastructure, build, deploy and create CICD pipeline using Vagrant, Ansible, Docker, Gitlab and Kubernetes

## Technology
* Vagrant
* Ansible
* Docker
* Kubernetes
* Gitlab-ci

![./images/infra-tool.png](./images/infra-tool.png?raw=true "./images/infra-tool.png")

## Infrastructure
![./images/setup.jpg](./images/setup.jpg?raw=true "./images/setup.jpg")

## CICD flow
![./images/cicd.png](./images/cicd.png?raw=true "./images/cicd.png")

## Project structure
* infra - Contains Vagrantfile and Ansible Playbook to setup infrastructure
* manifest - Contains different kubernetes manifest file of some example application
* src - Contains a simple API nodejs to build and deploy
* .gitlab-ci.yml - Gitlab CICD for simple API nodejs