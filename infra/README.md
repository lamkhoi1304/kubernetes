# Create a Kubernetes Cluster with Vagrant, Ansible, Kubeadm

## Setup Infrastructure
![./images/setup.jpg](./images/setup.jpg?raw=true "./images/setup.jpg")

## Cluster Information
* Kubernetes version: v1.23.1
* OS version: Ubuntu 20.04
* Container runtime: Containerd v1.6.7(latest)
* Container network interface: Flannel v0.19.1(latest)

## System Requirements
* 1 master has 2 CPUs and 2 GBs RAM (recommended)
* 3 workers, each worker has 2 CPUs and 2 GBs RAM (recommended)
* 1 nfs-server has 1 CPUs and 1 GBs RAM
* 1 runner-server has 2 CPUs and 2 GBs RAM (recommended)

## Setup Prerequisites
* A ssh key in .ssh/
* Virtualbox [install](https://linuxhint.com/install-setup-virtualbox-ubuntu-22-04/)
    ```
    $ sudo apt install virtualbox
    ```
* Vagrant [install](https://itslinuxfoss.com/install-vagrant-ubuntu-22-04/)
    ```
    $ sudo apt install vagrant
    ```
* Ansible [install](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-22-04)
    ```
    $ sudo apt install ansible
    ```
* Kubectl [install](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
    ```
    $ sudo apt-get update
    $ sudo apt-get install -y ca-certificates
    $ sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    $ echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
    $ sudo apt-get update
    $ sudo apt-get install -y kubectl
    ```
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```

## Provision VMs
```
$ vagrant up
```

## Create Kubernetes Cluster
```
$ ansible-playbook -i inventory main.yml
```

Download kubeconfig to local
```
$ mkdir ~/.kube-local
$ scp ci@192.168.56.11:/home/ci/.kube/config ~/.kube-local/config
$ chmod 600 ~/.kube-local/config
$ export KUBECONFIG=~/.kube-local/config
```
Verify cluster
```
$ kubectl get nodes
```

Deploy MetalLB
```
$ helm repo add metallb https://metallb.github.io/metallb
$ kubectl create ns metallb-system
$ helm install -n metallb-system metallb metallb/metallb
$ kubectl create -f ./metallb/values.yml
```

Deploy NFS Subdir External Provisioner
```
$ helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
$ kubectl create ns nfs-system
$ helm install -n nfs-system nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=192.168.56.31 --set nfs.path=/mnt/nfs_share
```

Deploy Metrics Server
```
$ helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
$ kubectl create ns metrics-system
$ helm install -n metrics-system metrics-server metrics-server/metrics-server --values ./metrics-server/values.yml
```

Deploy Ingress Nginx
```
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
$ kubectl create ns ingress-nginx
$ helm install -n ingress-nginx ingress-nginx ingress-nginx/ingress-nginx --values ./ingress-nginx/values.yml
```