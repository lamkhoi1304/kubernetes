# Deploy API to Kubernetes Cluster

## Setup Infrastructure
![./images/setup.png](./images/setup.png?raw=true "./images/setup.png")

## Image Information
* Backend: node:12.18-alpine
* Mongodb: mongo:latest

## Setup Prerequisites
* Create mongodb folder on NFS-Server for PersistentVolume
* Create wordpress namespace
    ```
    $ kubectl create ns api
    ```

## Setup Gitlab-Runner
Assign runner to project
```
Gitlab repository -> Settings -> CI/CD -> Runners -> Expand
```
```
$ sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```
* Enter the GitLab instance URL: https://gitlab.com/
* Enter the registration token: $REGISTRATION_TOKEN
* Enter a description for the runner: runner-1
* Enter tags for the runner (comma-separated):
* Enter optional maintenance note for the runner:
* Enter an executor: docker
* Enter the default Docker image: docker

Verify Gitlab-Runner
```
$ sudo gitlab-runner status
```
Enabling the privileged mode
```
$ sudo vim /etc/gitlab-runner/config.toml
Edit: privileged = true
$ sudo gitlab-runner restart
```
Troubleshooting
* This job is stuck because the project doesn’t have any runners online assigned to it.
```
Settings -> CI/CD -> Runners -> Expand -> Edit
```
![./images/runner.png](./images/runner.png?raw=true "./images/runner.png")

* Error: Error during connect: Post “http://docker:2375/v1.24/auth": dial tcp: lookup docker on 10.0.0.2:53: no such host
```
Enabling the privileged mode
```

*   Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
```
variables:
   DOCKER_TLS_CERTDIR: ""
```

## Mongodb
* Create volume
    ```
    $ kubectl apply -f mongodb-volume.yml
    ```
* Create statefulset
    ```
    $ kubectl apply -f mongodb-statefulset.yml
    ```
* Create service
    ```
    $ kubectl apply -f mongodb-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n api
    ```
    ![./images/mongodb.png](./images/mongodb.png?raw=true "./images/mongodb.png")

## Backend
* Create configmap
    ```
    kubectl apply -f backend-cm.yml
    ```
* Create deployment
    ```
    $ kubectl apply -f backend-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f backend-service.yml
    ```
* Create HPA
    ```
    $ kubectl apply -f backend-hpa.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n api
    ```
    ![./images/backend.png](./images/backend.png?raw=true "./images/backend.png")
* Access backend-svc EXTERNAL-IP to verify
    ```
    192.168.56.201:3000
    ```
    
## Test API using Postman
* /ping  \
    ![./images/ping.png](./images/ping.png?raw=true "./images/ping.png")
* /versionz \
    ![./images/version.png](./images/version.png?raw=true "./images/version.png")
* /register \
    ![./images/register.png](./images/register.png?raw=true "./images/register.png")
* /login \
    ![./images/login.png](./images/login.png?raw=true "./images/login.png")

## Test HPA using Fortio
Source: https://github.com/fortio/fortio
```
$ fortio load -qps 50 -t 60s "192.168.56.201:3000/ping"
```
![./images/fortio1.png](./images/fortio1.png?raw=true "./images/fortio1.png")
![./images/fortio2.png](./images/fortio2.png?raw=true "./images/fortio2.png")
![./images/fortio3.png](./images/fortio3.png?raw=true "./images/fortio3.png")