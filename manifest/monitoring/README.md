# Kubernetes Monitoring with Prometheus and Grafana

## Kube Prometheus
Using manifests fulder from this repository: [Here](https://github.com/prometheus-operator/kube-prometheus/tree/release-0.11)

## Setup CRDs
```
$ kubectl create -f ./manifests/setup/
```

## Setup Manifests
```
$ kubectl create -f ./manifests/
```

## Verify
```
$ kubectl get pods -n monitoring
```

## View Dashboards
```
$ kubectl -n monitoring port-forward svc/grafana 3000
```
```
Username: admin
Password: admin
```