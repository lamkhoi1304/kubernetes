# Deploy Java Webapp to Kubernetes Cluster
Source: https://gitlab.com/lamkhoi1304/webapp

## Setup Infrastructure
![./images/setup.png](./images/setup.png?raw=true "./images/setup.png")

## Image Information
* Maven: maven:3.5-jdk-8
* Tomcat: tomcat:8-jre11
* Mysql: mysql:5.7
* Rabbitmq: rabbitmq:latest
* Memcached: memcached:latest

## Setup Prerequisites
* Create webapp-mysql folder on NFS-Server for PersistentVolume
* Create wordpress namespace
    ```
    $ kubectl create ns webapp
    ```

## Setup Gitlab-Runner
Assign runner to project
```
Gitlab repository -> Settings -> CI/CD -> Runners -> Expand
```
Following [Maven](https://hub.docker.com/_/maven)
```
$ sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN --docker-volumes "/root/.m2:/root/.m2"
```
* Enter the GitLab instance URL: https://gitlab.com/
* Enter the registration token: $REGISTRATION_TOKEN
* Enter a description for the runner: runner-1
* Enter tags for the runner (comma-separated):
* Enter optional maintenance note for the runner:
* Enter an executor: docker
* Enter the default Docker image: docker

Verify Gitlab-Runner
```
$ sudo gitlab-runner status
```
Enabling the privileged mode
```
$ sudo vim /etc/gitlab-runner/config.toml
Edit: privileged = true
$ sudo gitlab-runner restart
```
Troubleshooting
* This job is stuck because the project doesn’t have any runners online assigned to it.
```
Settings -> CI/CD -> Runners -> Expand -> Edit
```
![./images/runner.png](./images/runner.png?raw=true "./images/runner.png")

* Error: Error during connect: Post “http://docker:2375/v1.24/auth": dial tcp: lookup docker on 10.0.0.2:53: no such host
```
Enabling the privileged mode
```

*   Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
```
variables:
   DOCKER_TLS_CERTDIR: ""
```

## Create Secret
```
$ kubectl apply -f secret.yml
```

## Mysql
* Create volume
    ```
    $ kubectl apply -f mysql-volume.yml
    ```
* Create configmap
    ```
    $ kubectl apply -f mysql-cm.yml
    ```
* Create deployment
    ```
    $ kubectl apply -f mysql-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f mysql-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n webapp
    ```
    ![./images/mysql.png](./images/mysql.png?raw=true "./images/mysql.png")

## Rabbitmq
* Create deployment
    ```
    $ kubectl apply -f mq-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f mq-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n webapp
    ```
    ![./images/mq.png](./images/mq.png?raw=true "./images/mq.png")

## Memcached
* Create deployment
    ```
    $ kubectl apply -f memcached-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f memcached-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n webapp
    ```
    ![./images/memcached.png](./images/memcached.png?raw=true "./images/memcached.png")


## Webapp
* Create deployment
    ```
    $ kubectl apply -f webapp-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f webapp-service.yml
    ```
* Create ingress
    ```
    $ kubectl apply -f webapp-ing.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n webapp
    ```
    ![./images/webapp.png](./images/webapp.png?raw=true "./images/webapp.png")
* Access url
    ```
    $ echo "192.168.56.201 webapp.example.com" | sudo tee -a /etc/hosts
    ```
