# Deploy Wordpress to Kubernetes Cluster

## Setup Infrastructure
![./images/setup.png](./images/setup.png?raw=true "./images/setup.png")

## Image Information
* Wordpress: wordpress:5.8-apache
* Mysql: mysql:5.7
* Adminer: adminer:latest

## Setup Prerequisites
* Create wp and mysql folder on NFS-Server for PersistentVolume
* Create wordpress namespace
    ```
    $ kubectl create ns wordpress
    ```

## Create Secret
Following [Wordpress](https://hub.docker.com/_/wordpress) and [Mysql](https://hub.docker.com/_/mysql)
* Kubernetes stores secrets as base64 encoded strings. You can create base64 with:
    ```
    $ echo -n "{{ Your whatever secret }}" | base64
    ```
```
$ kubectl apply -f secret.yml
```


## Mysql
* Create volume
    ```
    $ kubectl apply -f mysql-volume.yml
    ```
* Create deployment
    ```
    $ kubectl apply -f mysql-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply  -f mysql-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n wordpress
    ```
    ![./images/mysql.png](./images/mysql.png?raw=true "./images/mysql.png")

## Wordpress
* Create volume
    ```
    $ kubectl apply -f wordpress-volume.yml
    ```
* Create deployment
    ```
    $ kubectl apply -f wordpress-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f wordpress-service.yml
    ```
* Create ingress
    ```
    $ kubectl apply -f wordpress-ing.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n wordpress
    ```
    ![./images/wordpress.png](./images/wordpress.png?raw=true "./images/wordpress.png")
* Access url
    ```
    $ echo "192.168.56.201 wordpress.example.com" | sudo tee -a /etc/hosts
    ```

## Adminer
* Create deployment
    ```
    $ kubectl apply -f adminer-deployment.yml
    ```
* Create service
    ```
    $ kubectl apply -f adminer-service.yml
    ```
* Verify
    ```
    $ kubectl get all,pv,pvc -n wordpress
    ```
    ![./images/adminer.png](./images/adminer.png?raw=true "./images/adminer.png")
* Access adminer-svc EXTERNAL-IP to verify
    ```
    192.168.56.202:8080
    ```