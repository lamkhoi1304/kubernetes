# Simple API with Nodejs, Mongodb
Source: https://gitlab.com/maitrungduc1410/learning-docker

## Image Information
* Nodejs: node:12.18-alpine
* Mongodb: mongo:latest

## Quickstart
* Build image
    ```
    $ docker built -t registry.gitlab.com/lamkhoi1304/kubernetes:latest .
    ```
* Run docker-compose
    ```
    docker-compose up -d
    ```
* Exec npm run test
    ```
    docker-compose exec -T app npm run test
    ```

## Veryfi API
* /ping | GET
    ```
    $ curl localhost:3000/ping
    {"message":"Pong"}
    ```
* /versionz | GET
    ```
    $ curl localhost:3000/versionz
    {"message":"Version 1.0.2"}
    ```
* /register | POST
    ```
    $ curl -X POST localhost:3000/register -H 'Content-Type: application/json' -d '{"email":"test@gmail.com","password":"123456","displayName":"test"}'
    {"message":"success"}
    ```
* /login | POST
    ```
    $ curl -X POST localhost:3000/login -H 'Content-Type: application/json' -d '{"email":"test@gmail.com","password":"123456"}'
    {"_id":"630c89d907ce1d0024a1ed69","email":"test@gmail.com","displayName":"test","createdAt":"2022-08-29T09:41:45.314Z","updatedAt":"2022-08-29T09:41:45.314Z","__v":0}
    ```